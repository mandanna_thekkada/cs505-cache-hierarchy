#include "m3.h"

string ibin(int i,int sz)
{
        int rem;
        string temp;
        while(i>0)
        {
            rem=i%2;
            if(rem==1)
            temp="1"+ temp;
            else
            temp="0"+temp;

            i=i/2;
        }

        while(temp.length()<sz)
        {
            temp="0"+temp;
        }
       return temp;
}

cache::cache(int sz,int assoc,int blksz,int htime,int mtime )
    {
        int i=0;
        map<string,drow>::iterator mitr;
        size=sz;
        this->assoc=assoc;
        blk_size=blksz;
        hit_time=htime;
        miss_time=mtime;

        rows=size/(blk_size*assoc);

    }

int cache::cache_access(string ppn,string poff,char dir,int level)               //dir denotes whether it is a read access or write access
{
    map<string,drow>::iterator itr;
    string ind = poff.substr(0,poff.length()-log2(blk_size));       //finding index from page offset
    tot_mem_refs++;
    //cout<<"\nindex of incoming address = "<<ind<<"for level = "<<level<<endl;

    itr=data.find(ind);
    num_of_access++;

    if(itr==data.end())                              //if index not present insert it...i.e. compulsory miss
    {
        //cout<<"case index does not exist"<<endl;
        drow dtemp;
        dtemp.dmax=2;

        dentry t_entr;
        t_entr.lru=1;
        t_entr.tag=ppn;
        t_entr.valid=true;

        if(dir=='R')
        t_entr.dirty=false;
        else
        t_entr.dirty=true;

        dtemp.dentry_arr.push_front(t_entr);

        data[ind]=dtemp;
        clk+=miss_time;

        return 0;
    }
    else
    {
        //cout<<"case index found"<<endl;
        //perform tag comparison
        list<dentry>::iterator itr;


        // cache hit
        for(itr=data[ind].dentry_arr.begin();itr!=data[ind].dentry_arr.end();itr++)
        {
            if(((*itr).tag == ppn )&&((*itr).valid==true))                //hit
            {
            //    cout<<"case hit for ppn = "<<ppn<<endl;
                clk+=hit_time;
                tot_hits++;

                if(dir=='W')
                (*itr).dirty=true;               //overwriting dirty bit because it is a write hit

                (*itr).lru=data[ind].dmax;
                data[ind].dmax++;
                clk+=hit_time;
                num_of_hits++;
                return 1;
            }
        }

        //tag was not found hence it is a miss........

        if(data[ind].dentry_arr.size() < assoc)                    //can be compulsory miss
        {
            //cold miss .. set has not been completely filled yet
          //  cout<<"set not full"<<endl;

            dentry t_entr;
            t_entr.tag=ppn;

            t_entr.lru=data[ind].dmax;
            data[ind].dmax++;

            if(dir=='W')                    //setting dirty bit
            t_entr.dirty=true;
            else
            t_entr.dirty=false;

            t_entr.valid=true;

            data[ind].dentry_arr.push_front(t_entr);
            clk+=miss_time;
        //    cout<<"index in incomplete set = "<<ind<<"size = "<<data[ind].dentry_arr.size()<<endl;

            return 0;
        }
        else                                                      //conflict miss
        {
      //      cout<<" case conflict miss case has been entered for index = "<<ind<<endl;
            int min=-1;

            list<dentry>::iterator itr2;
                                                //perform lru
            for(itr=data[ind].dentry_arr.begin();itr!=data[ind].dentry_arr.end();itr++)
            {
                if(min==-1)
                {
                    min=(*itr).lru;
                    itr2=itr;
                }
                else if(min > (*itr).lru)
                {

                    min =  (*itr).lru;
                    itr2=itr;
                }

            }

            //cout<<"tag of victim = "<<(*itr2).tag<<endl;

            if((level==1) && ((*itr2).dirty == true))         //performing write back to l2 cache
            {
                write_back=1;
                wb_addr=(*itr2).tag+ind+"000000";                      //forming the block address where write back has to occur

                //performing l2-cache access for write_back
                string wb_vpn=wb_addr.substr(0,vmem_sz-(log2(l2_cache->rows)+log2(l2_cache->blk_size)));
                string wb_poff=wb_addr.substr(vmem_sz-(log2(l2_cache->rows)+log2(l2_cache->blk_size)),(log2(l2_cache->rows)+log2(l2_cache->blk_size)));

                l2_cache->cache_access(wb_vpn,wb_poff,'W',2);
            }

            //storing data which may be used to perform back_inv
            if(level==2)
            back_inv_str=(*itr2).tag+ind+"000000";

            //writing new data
            (*itr2).tag=ppn;
            (*itr2).lru=data[ind].dmax;
            data[ind].dmax++;

            if(dir=='W')                    //setting dirty bit
            (*itr2).dirty=true;
            else
            (*itr2).dirty=false;

            (*itr2).valid=true;
            clk+=miss_time;
            return 0;
        }

    }
}

void cache::print_cache()
{
    map<string,drow>::iterator itr;
    for(itr=data.begin();itr!=data.end();itr++)
    {
        cout<<(*itr).first<<endl;                        //printing index
        list<dentry>::iterator ditr;

        for(ditr=(*itr).second.dentry_arr.begin();ditr!=(*itr).second.dentry_arr.end();ditr++)
        {
            cout<<(*ditr).tag<<"   "<<(*ditr).lru<<"    "<<(*ditr).valid<<"    "<<(*ditr).dirty<<endl;
        }
    }
}

tlb::tlb()
{

}

tlb::tlb(int n)
{
    n_entr=n;
    max=0;
}

string tlb::tlb_access(string tag)
{
    if(entr_list.size()==0)
    {

        dentry temp;
        temp.tag=tag;
        temp.lru=1;
        max=2;
        temp.valid=true;
        //cout<<"\nfirst element tag = "<<temp.tag<<endl;
        entr_list.push_front(temp);
        list<dentry>::iterator itr;
        itr=entr_list.begin();

        clk+=l2_htime;
        //cout<<"\nfirst element tag from list= "<<(*itr).tag<<"list size = "<<entr_list.size()<<endl;

        return miss;
    }
   else if(entr_list.size() < (n_entr))            //tlb still not completely full
    {
        //cout<<"\n tlb access case 2"<<endl;
        list<dentry>::iterator itr;
        int flag=0;
        for(itr=entr_list.begin();itr!=entr_list.end();itr++)
        {
            if((*itr).tag==tag)               //tlb hit
            {
                (*itr).lru=max;
                max++;
                flag=1;
                return (*itr).tag;
            }
        }

        if(flag==0)                      //tlb not yet full and tlb miss
        {
            dentry temp;
            temp.tag=tag;
            temp.lru=max;
            max++;
            temp.valid=true;
            entr_list.push_front(temp);
            clk+=l2_htime;

            return miss;

        }
    }
    else
    {
      //  cout<<"\n tlb access case 3"<<endl;
        list<dentry>::iterator itr;
        int flag=0;
        for(itr=entr_list.begin();itr!=entr_list.end();itr++)
        {
            if((*itr).tag==tag)               //tlb hit
            {
                (*itr).lru=max;
                max++;
                flag=1;
                return (*itr).tag;
            }
        }

        if(flag==0)                                   //tlb full and tlb miss
        {
            int lru_min=0;
            list<dentry>::iterator itr;
            list<dentry>::iterator itr2;

            for(itr=entr_list.begin(),itr2=entr_list.begin();itr!=entr_list.end();itr++)
            {
                if(lru_min==0)               //tlb hit
                {
                    lru_min=(*itr).lru;
                    itr2=itr;
                }
                else
                {
                    if((*itr).lru<lru_min)
                    {
                        lru_min=(*itr).lru;
                        itr2=itr;
                    }
                }
            }

            (*itr2).tag=tag;
            (*itr2).lru=max;
            max++;
            clk+=l2_htime;

            return miss;
        }
    }
    //cout<<"\nexiting tlbaccess"<<endl;
}


instr::instr(string str_instr)
        {
            int i=0;

            char * tstr=strtok((char *)str_instr.c_str(),"       = \n");
            in_addr=hextobin(tstr);

            a_ct=0;
            for(i=0;i<nmem;i++)
            {
                addr_list[i].addr="\0";
                addr_list[i].dir='\0';
            }

            tstr=strtok(NULL,"       = \n");               //skipping the opcode


            while(1)
                {

                       tstr=strtok(NULL,"       = \n");
                       if(tstr!=NULL)
                       {

                            if(!strcmp("SOURCE",tstr))
                            {
                                tstr=strtok(NULL," \n");
                                if(tstr[0]!='r')
                                {
                                    addr_list[a_ct].addr=hextobin(tstr);
                                    addr_list[a_ct].dir='R';
                                    a_ct++;
                                }

                            }
                            else if(!strcmp("DEST",tstr))
                            {
                                tstr=strtok(NULL," \n");
                                if(tstr[0]!='r')
                                {
                                    addr_list[a_ct].addr=hextobin(tstr);
                                    addr_list[a_ct].dir='W';
                                    a_ct++;
                                }
                            }
                       }
                       else
                       break;
                }

        }

 void instr::print_instr()
        {
            int i;
            //cout<<"\n"<<in_addr<<endl;

            for(i=0;i<a_ct;i++)
                {
                    cout<<addr_list[i].addr<<"  "<<addr_list[i].dir<<" "<<endl;
                }

            //cout<<"\na_ct = "<<a_ct<<endl;
        }



string hextobin(char* str)                //returns a binary string from a hexadecimal string address in 0x... format
{
   int i=0;
   string temp;
  // cout<<"\n length = "<<strlen(str)<<endl;

   if(strlen(str)<10)  //padding zero bits at msb
   {
        while(i<(10-strlen(str)))
        {
            temp+="0000";
            i++;
        }
   }

    i=2;
   for(i=2;i<strlen(str);i++)
   {
       temp+=hexbin[str[i]];
   }
   return temp;
}


void fetch_instr()
{

 if(!ipfile.eof())
    {
        read_count++;
        if(read_count>=read_lim)
        endflag=1;

        getline(ipfile,line);
        if(strcmp(line.c_str(),"$$$"))
        {
            instr temp_instr =instr(line);

            /*------------CACHE AND TLB ACCESS FOR INSTRUCTION ADDRESS ----------------------*/

            i_cur_vpn = temp_instr.in_addr.substr(0,vmem_sz-ipage);       //removing the virtual page number
            i_page_off= temp_instr.in_addr.substr(vmem_sz-ipage,ipage);   //removing the page offset

            //cout<<"\nCurrent vpn = "<<i_cur_vpn<<endl;
            //cout<<"\nCurrent page offdset = "<<i_page_off<<endl;


            string ret_val = itlb_ptr->tlb_access(i_cur_vpn);            //accessing i-tlb
            //cout<<"return from i-tlb"<<endl;
            int ret=l1_i_cache->cache_access(i_cur_vpn,i_page_off,'R',1);        //accessing the l1-i-cache to fetch the instruction

            //cout<<"return from l1-i-cache"<<endl;
            if(ret == 0)                                               // l1-i-cache miss ..access l2 - cache
            {
                //have to re-perform page offset and tags depending on associativity of l2 nd all..
                i_cur_vpn = temp_instr.in_addr.substr(0,vmem_sz-(log2(l2_cache->rows)+log2(l2_cache->blk_size)));

                i_page_off= temp_instr.in_addr.substr(vmem_sz-(log2(l2_cache->rows)+log2(l2_cache->blk_size)),(log2(l2_cache->rows)+log2(l2_cache->blk_size)));   //removing the page offset

                l2_cache->cache_access(i_cur_vpn,i_page_off,'R',2);

      //          cout<<"In l2 cache index = "<<log2(l2_cache->rows)<<" block offset = "<<log2(l2_cache->blk_size)<<endl;
            }
            /*---------------------------------------------------------------------------------*/

            /*-----------------------------GOING THROUGH DATA REFERENCES-----------------------*/

        //    cout<<"\n BEGINNING DATA ACCESSES for instr"<<temp_instr.in_addr<<endl;

            int i=0;

            for(i=0;i<temp_instr.a_ct;i++)
            {
                    d_cur_vpn = temp_instr.addr_list[i].addr.substr(0,vmem_sz-dpage);       //removing the virtual page number
                    d_page_off= temp_instr.addr_list[i].addr.substr(vmem_sz-dpage,dpage);   //removing the page offset

            //        cout<<"Current data address = "<<temp_instr.addr_list[i].addr<<endl;
          //          cout<<"Current data vpn = "<<d_cur_vpn<<endl;
              //      cout<<"Current data page offset = "<<d_page_off<<endl;

                    string ret_val = dtlb_ptr->tlb_access(d_cur_vpn);

                    char temp_dir=temp_instr.addr_list[i].dir;
                    if(temp_dir=='R')
                    ret=l1_d_cache->cache_access(d_cur_vpn,d_page_off,'R',1);
                    else
                    ret=l1_d_cache->cache_access(d_cur_vpn,d_page_off,'W',1);

                    if(ret==0)   //if miss in l1 check in l2
                    {
                        d_cur_vpn = temp_instr.addr_list[i].addr.substr(0,vmem_sz-(log2(l2_cache->rows)+log2(l2_cache->blk_size)));

                        d_page_off= temp_instr.addr_list[i].addr.substr(vmem_sz-(log2(l2_cache->rows)+log2(l2_cache->blk_size)),(log2(l2_cache->rows)+log2(l2_cache->blk_size)));   //removing the page offset

                        if(temp_dir=='R')
                        l2_cache->cache_access(d_cur_vpn,d_page_off,'R',2);
                        else
                        l2_cache->cache_access(d_cur_vpn,d_page_off,'W',2);

                    }

            }
            //cout<<"\nEND OF DATA ACCESSES"<<endl;
            /*-----------------------------------------------------------------------------------*/
        }
        else
        {
        cout<<"END OF FILE"<<endl;
        ipfile.close();
        endflag=1;
        }
    }

}



void cycle()
{
    while(endflag!=1)
    {
        fetch_instr();
        clk++;
    }
}

void init()
{
    ipfile.open("main_fulli.out");

    hexbin['0']="0000";
    hexbin['1']="0001";
    hexbin['2']="0010";
    hexbin['3']="0011";
    hexbin['4']="0100";
    hexbin['5']="0101";
    hexbin['6']="0110";
    hexbin['7']="0111";
    hexbin['8']="1000";
    hexbin['9']="1001";
    hexbin['a']="1010";
    hexbin['b']="1011";
    hexbin['c']="1100";
    hexbin['d']="1101";
    hexbin['e']="1110";
    hexbin['f']="1111";

    itlb_ptr=new tlb(itlb_sz);         //creating instruction tlb
    dtlb_ptr=new tlb(dtlb_sz);         //creating data tlb

    l1_i_cache=new cache(l1_i_sz,l1_i_assoc,l1_i_block,l1_i_htime,l1_i_mtime);  //creating l1-i-cache
    l1_d_cache=new cache(l1_d_sz,l1_d_assoc,l1_d_block,l1_d_htime,l1_d_mtime);  //creating l1-d-cache

    l2_cache=new cache(l2_sz,l2_assoc,l2_block,l2_htime,l2_mtime);              //creating l2 cache

    ipage=(log(l1_i_block)/log(2))+(log(l1_i_cache->rows)/log(2));              //calculating page offset for i-caches
    dpage=(log(l1_d_block)/log(2))+(log(l1_d_cache->rows)/log(2));              //calulating page offset for d-caches


}

void tlb::printtlb()
{
  list<dentry>::iterator itr;

  for(itr=entr_list.begin();itr!=entr_list.end();itr++)
  {
      cout<<"\n  "<<(*itr).tag<<"  "<<(*itr).lru<<"   "<<(*itr).valid<<" "<<(*itr).tag.length()<<endl;
  }
}
int main(int argc,char *argv[])
{


    itlb_sz=atoi(argv[1]);
    dtlb_sz=atoi(argv[2]);
    l1_i_sz=atoi(argv[3]);
    l1_d_sz=atoi(argv[4]);
    l2_sz=atoi(argv[5]);
   // cout<<"itlb size = "<<itlb_sz<<endl;
    init();

    while(endflag!=1)
    {
        cycle();
    }
    cout<<"\n printing dtlb"<<endl;
    dtlb_ptr->printtlb();

    cout<<"tlb size = "<<dtlb_ptr->entr_list.size()<<endl;
    cout<<"\npage size = "<<ipage<<endl;

    cout<<"\nPrinting the l1-i-cache"<<endl;
    l1_i_cache->print_cache();

    cout<<"\nPrinting the l1-d-cache"<<endl;
    l1_d_cache->print_cache();

    cout<<"\nPrinting the l2-cache"<<endl;
    l2_cache->print_cache();

    cout<<"\n i-cache num of sets = "<<l1_i_cache->data.size()<<endl;

    cout<<"dpage = "<<dpage<<endl;
    cout<<"rows in d-cache = "<<l1_d_cache->rows<<endl;


    cout<<"total number of memory references = "<<tot_mem_refs<<endl;

    cout<<"clock = "<<clk<<endl;
    cout<<"hit rate = "<<(tot_hits/tot_mem_refs)<<endl;

    double temp_rate=(double)(l1_i_cache->num_of_hits)/(l1_i_cache->num_of_access);
    cout<<"l1 - i - cache accesses "<<(l1_i_cache->num_of_access)<<endl;
     cout<<"l1 - i - cache hits "<<(l1_i_cache->num_of_hits)<<endl;
    cout<<"l1-i-cache hit rate = "<<(double)temp_rate<<endl;

    temp_rate=(double)(l1_d_cache->num_of_hits)/(l1_d_cache->num_of_access);
    cout<<"l1 - d - cache accesses "<<(l1_d_cache->num_of_access)<<endl;
     cout<<"l1 - d - cache hits "<<(l1_d_cache->num_of_hits)<<endl;
    cout<<"l1-d-cache hit rate = "<<(double)temp_rate<<endl;

    temp_rate=(double)(l2_cache->num_of_hits)/(l2_cache->num_of_access);
     cout<<"l2 - cache accesses "<<(l2_cache->num_of_access)<<endl;
     cout<<"l2 - cache hits "<<(l2_cache->num_of_hits)<<endl;
    cout<<"l2 - cache hit rate = "<<(double)temp_rate<<endl;

     /*temp_rate=(double)(l2_cache->num_of_hits)/(tot_mem_refs);
     cout<<"global cache accesses "<<(tot_mem_refs)<<endl;
     cout<<"l2 - cache hits "<<(l2_cache->num_of_hits)<<endl;*/
     double l2_misses=(double)(l2_cache->num_of_access - l2_cache->num_of_hits);
     double l1_i_misses=(double)(l1_i_cache->num_of_access - l1_i_cache->num_of_hits);
     double l1_d_misses=(double)(l1_d_cache->num_of_access - l1_d_cache->num_of_hits);

    cout<<" l1 - i - cache misses = "<<(double)l1_i_misses<<endl;
    cout<<" l1 - d - cache misses = "<<(double)l1_d_misses<<endl;
    cout<<" l2 - cache misses = "<<(double)l2_misses<<endl;
    return 0;
}

