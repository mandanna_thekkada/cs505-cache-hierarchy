#include<iostream>
#include<cstdlib>
#include<queue>
#include<fstream>
#include<cstring>
#include<map>
#include<list>
#include<sstream>
#include<stdio.h>
#include<malloc.h>
#include<math.h>
#define nmem 10
#define l1_i_htime 1
#define l1_d_htime 1
#define l1_i_mtime 20
#define l1_d_mtime 20
#define l2_htime 20
#define l2_mtime 100
#define l1_i_assoc 2
#define l1_d_assoc 4
#define l1_i_block 64
#define l1_d_block 64
#define vmem_sz 32
#define l2_assoc 16
#define l2_block 64
#define read_lim 1000000
using namespace std;

int read_count=0;
ifstream ipfile;
string line;
int clk=0;
int endflag=0;
string miss="miss";
map<char,string>hexbin;



//parameters
int itlb_sz=0;
int dtlb_sz=0;
int l1_i_sz=0;
int l1_d_sz=0;
int l2_sz=0;


struct address
{
    string addr;
    char dir;
};

class instr
{
    public:
    string in_addr;

    address addr_list[nmem];
    int a_ct;                      //count of source operands

    instr();
    instr(string str_instr);
    void print_instr();

};

struct dentry
{
    string tag;
    long lru;
    bool valid;
    bool dirty;
};

struct drow
{
    list<dentry>dentry_arr;
    int dmax;

};

typedef struct dentry dentry;
typedef struct drow drow;

class cache
{
    public:
    int assoc;
    int size;
    int blk_size;
    int rows;             //number of sets
    map<string,drow>data;
    int hit_time;
    int miss_time;

    int num_of_hits;
    int num_of_access;

    cache();
    cache(int sz,int assoc,int blksz,int htime,int mtime);
    int cache_access(string ppn,string poff,char dir,int level);
    void print_cache();

};

class tlb
{
    public:
   int n_entr;
   list<dentry>entr_list;
   long max;

   tlb();
   tlb(int n);

   string tlb_access(string tag);
   void printtlb();

};

//components of cache hierarchy

tlb* dtlb_ptr;                //pointer to data tlb
tlb* itlb_ptr;                //pointer to instruction tlb
cache *l1_d_cache;            //pointer to l1 data cache
cache *l1_i_cache;            //pointer to l1 instruction cache
int ipage;                    //number of bits for ipage offset
int dpage;                    //number of bits for dpage offset
string i_cur_vpn;             //current virtual page number for i-cache
string i_page_off;            //current page offset for i-cache

string d_cur_vpn;             //current virtual page number for d-cache
string d_page_off;            //current page offset for d-cache

cache *l2_cache;              //pointer to l2-unified-cache
int write_back=0;             //value to denote if write back to l2 cache is required
string wb_addr;               //address where write back to l2 should occur
int back_inv=0;               //flag to denote when back invalidate is performed
string back_inv_str;

//global functions
string hextobin(char* str);                //returns a binary string from a hexadecimal string address in 0x... format
string ibin(int n,int sz);                 //returns n as binary representation of sz bits


double tot_mem_refs=0;
double tot_hits=0;                            //used to measure hit rate
